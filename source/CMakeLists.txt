# --------------------------------------------------------------------

get_filename_component(LIB_INCLUDE_PATH "." ABSOLUTE)
get_filename_component(LIB_SOURCE_PATH "./twist" ABSOLUTE)

# --------------------------------------------------------------------

# Sources

set(LIB_SOURCES
        "${LIB_SOURCE_PATH}/build.cpp")

CollectCxxSources("${LIB_SOURCE_PATH}/wheels" LIB_SOURCES)

if(TWIST_SIM)
    CollectCxxSources("${LIB_SOURCE_PATH}/rt/sim" LIB_SOURCES)
    CollectCxxSources("${LIB_SOURCE_PATH}/mod/sim" LIB_SOURCES)
else()
    CollectCxxSources("${LIB_SOURCE_PATH}/rt/thr" LIB_SOURCES)
    CollectCxxSources("${LIB_SOURCE_PATH}/mod/thr" LIB_SOURCES)
endif()

CollectCxxSources("${LIB_SOURCE_PATH}/mod/cross" LIB_SOURCES)

CollectCxxSources("${LIB_SOURCE_PATH}/test" LIB_SOURCES)

# --------------------------------------------------------------------

# Library

add_library(twist STATIC ${LIB_SOURCES})

target_include_directories(twist PUBLIC ${LIB_INCLUDE_PATH})

target_link_libraries(twist PUBLIC
        wheels
        fmt
        function2
        pthread)

if(TWIST_SIM)
    target_link_libraries(twist PUBLIC sure)
endif()

target_link_libraries(twist PRIVATE backward)
if(TWIST_SIM_DEBUG)
    target_link_libraries(twist PRIVATE bfd dl)
endif()

# --------------------------------------------------------------------

# Compile definitions

if(TWIST_FAULTY)
    target_compile_definitions(twist PUBLIC __TWIST_FAULTY__=1)

    if(TWIST_FAULT_PLACEMENT STREQUAL "BEFORE")
        target_compile_definitions(twist PUBLIC __TWIST_INJECT_FAULT_BEFORE__=1)
    elseif(TWIST_FAULT_PLACEMENT STREQUAL "AFTER")
        target_compile_definitions(twist PUBLIC __TWIST_INJECT_FAULT_AFTER__=1)
    elseif(TWIST_FAULT_PLACEMENT STREQUAL "BOTH")
        target_compile_definitions(twist PUBLIC
                __TWIST_INJECT_FAULT_BEFORE__=1
                __TWIST_INJECT_FAULT_AFTER__=1)
    else()
        message(FATAL_ERROR "Invalid fault placement")
    endif()
endif()

if(TWIST_SIM)
    target_compile_definitions(twist PUBLIC __TWIST_SIM__=1)

    if (TWIST_SIM_ISOLATION)
        target_compile_definitions(twist PUBLIC __TWIST_SIM_ISOLATION__=1)

        if (TWIST_SIM_FIXED_USER_MEMORY)
            target_compile_definitions(twist PUBLIC __TWIST_SIM_FIXED_USER_MEMORY_MAPPING__=1)
        endif()
    endif()

    if (TWIST_SIM_DEBUG)
        target_compile_definitions(twist PUBLIC __TWIST_SIM_DEBUG__=1)
    endif()


    if (TWIST_SIM_VECTORIZE_CLOCKS)
        target_compile_definitions(twist PUBLIC __TWIST_SIM_VECTORIZE_CLOCKS__=1)
    endif()
endif()

if (TWIST_ATOMIC_WAIT)
    target_compile_definitions(twist PUBLIC __TWIST_ATOMIC_WAIT__=1)
endif()

if (TWIST_NOISY_BUILD)
    target_compile_definitions(twist PUBLIC __TWIST_NOISY_BUILD__=1)
endif()
