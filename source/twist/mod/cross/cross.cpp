#include <twist/mod/cross.hpp>

#include <fmt/core.h>

#if defined(__TWIST_SIM__)

#include <twist/mod/sim.hpp>

namespace twist::cross {

void Run(sim::MainRoutine main) {
  auto params = sim::Simulator::Params{};
  // Consistent with mt::Run
  params.crash_on_abort = true;

  sim::sched::RandomScheduler random{};
  sim::Simulator simulator{&random, params};
  auto result = simulator.Run(main);

  if (result.Failure()) {
    fmt::println("Simulation failure: {}", result.std_err);
    std::abort();
  }
}

}  // namespace twist::cross

#else

#include <twist/mod/thr.hpp>

namespace twist::cross {

void Run(thr::TestRoutine test) {
  thr::Run(test);
}

}  // namespace twist::run

#endif
