#pragma once

#include <twist/rt/cap/wait/spin.hpp>

namespace twist::ed {

// Busy waiting

// Usage: examples/spin/main.cpp

/*
 * void SpinLock::Lock() {
 *   // One-shot
 *   twist::ed::SpinWait spin_wait;
 *
 *   while (locked_.exchange(true)) {
 *     spin_wait();  // operator()() <- exponential backoff
 *   }
 * }
 *
 */

using rt::cap::SpinWait;
using rt::cap::CpuRelax;

}  // namespace twist::ed
