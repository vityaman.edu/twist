#include "interrupt.hpp"
#include "preemption_guard.hpp"
#include "spurious.hpp"
#include "spin_wait.hpp"

#include <twist/rt/sim/system/simulator.hpp>

#include <twist/rt/sim/user/library/std_like/atomic.hpp>
#include <twist/ed/static/var.hpp>

namespace twist::rt::sim {

namespace user::scheduler {

struct Contention {
  library::std_like::Atomic<uint64_t> atom;

  Contention(wheels::SourceLocation source_loc = wheels::SourceLocation::Current())
      : atom(0, source_loc) {
  }
};

void Interrupt(wheels::SourceLocation call_site) {
  // TODO: better impl
  TWISTED_STATIC_VAR(Contention, interrupt);
  interrupt->atom.store(1, std::memory_order::relaxed, call_site);
}

PreemptionGuard::PreemptionGuard() {
  status_ = system::Simulator::Current()->Preemptive(false);
}

PreemptionGuard::~PreemptionGuard() {
  system::Simulator::Current()->Preemptive(status_);
}

bool SpuriousTryFailure() {
  return system::Simulator::Current()->SysSpuriousTryFailure();
}

bool IsModelChecker() {
  return system::Simulator::Current()->Scheduler()->IsModelChecker();
}

size_t SpinWaitYieldThreshold() {
  return system::Simulator::Current()->SpinWaitYieldThreshold();
}

}  // namespace scheduler

}  // namespace twist::rt::sim
