#pragma once

#include <twist/rt/sim/user/library/std_like/atomic_base.hpp>

#include <twist/rt/sim/user/assist/sharable.hpp>

namespace twist::rt::sim {

namespace user::library::std_like {

template <typename T>
class Atomic : public AtomicBase<T> {
  using Base = AtomicBase<T>;

 public:
  Atomic(wheels::SourceLocation source_loc = wheels::SourceLocation::Current())
      : Base(source_loc) {
  }

  Atomic(T value, wheels::SourceLocation source_loc = wheels::SourceLocation::Current())
      : Base(value, source_loc) {
    static_assert(sizeof(Atomic<T>) == sizeof(T));
  }

  T operator=(T new_value) {
    Base::store(new_value);
    return new_value;
  }
};

}  // namespace user::library::std_like

namespace user::assist {

template <typename T>
struct Sharable<library::std_like::Atomic<T>> {
  static const bool kStatus = false;
};

}  // namespace user::assist

}  // namespace twist::rt::sim
