#pragma once

#include <cstdlib>

#include <fmt/core.h>

namespace twist::rt::sim {

namespace user::library::fmt {

class BufWriter {
 public:
  BufWriter(char* buf, size_t size)
      : start_(buf), buf_(buf), size_(size) {
  }

  bool IsFull() const {
    return size_ == 0;
  }

  template <typename... Args>
  void FormatAppend(::fmt::format_string<Args...> format_str, Args&&... args) {
    auto written = ::fmt::format_to_n(buf_, size_, format_str, ::std::forward<Args>(args)...);

    buf_ += written.size;
    size_ -= written.size;
    written_ += written.size;
  }

  size_t Written() const {
    return written_;
  }

  std::string_view StringView() const {
    return {start_, Written()};
  }

 private:
  char* start_;
  char* buf_;
  size_t size_;
  size_t written_ = 0;
};

}  // namespace user::library::fmt

}  // namespace twist::rt::sim
