#pragma once

#include "../fwd.hpp"

namespace twist::rt::sim {

namespace system {

void Trampoline(Thread*);

}  // namespace system

}  // namespace twist::rt::sim
