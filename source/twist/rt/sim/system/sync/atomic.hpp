#pragma once

#include "../futex/wait_queue.hpp"

#include "action.hpp"
#include "clock.hpp"
#include "mo.hpp"

#include <cstdint>
#include <optional>

namespace twist::rt::sim {

namespace system::sync {

struct AtomicStore {
  VectorClock clock;
  uint64_t value;
};

struct AtomicDtor {
  //
};

struct AtomicVar {
  wheels::SourceLocation source_loc;
  AtomicStore last_store;
  WaitQueue futex;
  std::optional<AtomicDtor> dtor;
};

}  // namespace system::sync

}  // namespace twist::rt::sim
