#include "model.hpp"

#include "../abort.hpp"

#include <twist/wheels/format.hpp>

#include <wheels/core/compiler.hpp>

#include <fmt/core.h>

namespace twist::rt::sim {

namespace system::sync {

static void InvalidAccess(std::string err) {
  AbortIter(Status::InvalidMemoryAccess, std::move(err));
  WHEELS_UNREACHABLE();
}

// Atomics

void DeadAtomicVarAccess(AtomicVar* atomic, Action* action) {
  auto err = fmt::format("Accessing destroyed atomic: addr = {:#x}, source location = {}, operation = {}, access source location = {}",
                         (uintptr_t)action->loc, atomic->source_loc, action->op, action->source_loc);
  InvalidAccess(std::move(err));
}

void UnknownAtomicVarAccess(Action* action) {
  auto err = fmt::format("Invalid atomic access: addr = {:#x}, operation = {}, source location = {}",
                         (uintptr_t)action->loc, action->op, action->source_loc);
  InvalidAccess(std::move(err));
}

void UnknownAtomicVarAccess(void* loc) {
  auto err = fmt::format("Invalid atomic access: addr = {:#x}", (uintptr_t)loc);
  InvalidAccess(std::move(err));
}

// Shared

void DeadSharedVarAccess(NonAtomicVar* /*shared*/, Access* access) {
  auto err = fmt::format("Accessing destroyed shared variable: addr = {:#x}, access source location = {}", (uintptr_t)access->loc, access->source_loc);
  InvalidAccess(std::move(err));
}

void UnknownSharedVarAccess(Access* access) {
  auto err = fmt::format("Invalid shared variable access: addr = {:#x}, source location = {}", (uintptr_t)access->loc, access->source_loc);
  InvalidAccess(std::move(err));
}

}  // namespace system::sync

}  // namespace twist::rt::sim
