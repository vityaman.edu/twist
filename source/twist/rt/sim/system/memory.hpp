#pragma once

#if defined(__TWIST_SIM_ISOLATION__)

#include "memory/isolated/memory.hpp"

namespace twist::rt::sim {

namespace system {

// User memory isolated from system memory
using Memory = memory::isolated::Memory;

using Stack = memory::isolated::Stack;

}  // namespace system

}  // namespace twist::rt::sim

#else

#include "memory/shared/memory.hpp"

namespace twist::rt::sim {

namespace system {

// Memory shared between user and simulator (kernel)
using Memory = memory::shared::Memory;

using Stack = memory::shared::Stack;

}  // namespace system

}  // namespace twist::rt::sim

#endif
