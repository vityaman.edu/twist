#pragma once

#include <cstdint>

namespace twist::rt::thr {

namespace futex {

struct WakeKey {
  uint32_t* addr;
};

}  // namespace futex

}  // namespace twist::rt::thr
