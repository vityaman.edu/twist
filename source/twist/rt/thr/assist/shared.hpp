#pragma once

namespace twist::rt::thr {

namespace assist {

template <typename T>
class SharedWriteView {
 public:
  SharedWriteView(T* v)
      : v_(v) {
  }

  T& operator*()  {
    Write();
    return *v_;
  }

  T* operator->() {
    Write();
    return v_;
  }

 private:
  void Write() {
    // No-op
  }

 private:
  T* v_;
};

template <typename T>
class SharedReadView {
 public:
  SharedReadView(const T* v)
      : v_(v) {
  }

  const T& operator*() const {
    Read();
    return *v_;
  }

  const T* operator->() const {
    Read();
    return v_;
  }

  operator T() const {
    Read();
    return *v_;
  }

 private:
  void Read() const {
    // No-op
  }

 private:
  const T* v_;
};

template <typename T>
class Shared {
 public:
  template <typename... Args>
  Shared(Args&& ... a)
      : val_{std::forward<Args>(a)...} {
  }

  // Non-copyable
  Shared(const Shared&) = delete;
  Shared& operator=(const Shared&) = delete;

  // Non-movable
  Shared(Shared&&) = delete;
  Shared& operator=(Shared&&) = delete;

  ~Shared() {
    // No-op
  }

  T& operator*() {
    return val_;
  }

  const T& operator*() const {
    return val_;
  }

  operator T() const {
    return val_;
  }

  T* operator->() {
    return &val_;
  }

  const T& operator->() const {
    return &val_;
  }

  void Write(T new_val) {
    val_ = std::move(new_val);
  }

  T Read() const {
    return val_;
  }

  SharedReadView<T> ReadView() const {
    return {&val_};
  }

  SharedWriteView<T> WriteView() {
    return {&val_};
  }

 private:
  void* Loc() const {
    return (void*)&val_;
  }

 private:
  T val_;
};

}  // namespace assist

}  // namespace twist::rt::thr
