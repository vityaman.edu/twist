#pragma once

#if defined(__TWIST_SIM__)

#include <twist/rt/sim/user/assist/preempt.hpp>

namespace twist::rt::cap::assist {

using rt::sim::user::assist::PreemptionPoint;

}  // namespace twist::rt::cap::assist

#elif defined(__TWIST_FAULTY__)

#include <twist/rt/thr/fault/assist/preempt.hpp>

namespace twist::rt::cap::assist {

using rt::thr::fault::assist::PreemptionPoint;

}  // namespace twist::rt::cap::assist

#else

#include <twist/rt/thr/assist/preempt.hpp>

namespace twist::rt::cap::assist {

using rt::thr::assist::PreemptionPoint;

}  // namespace twist::rt::cap::assist

#endif
